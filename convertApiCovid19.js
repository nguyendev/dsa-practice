const a = {
  timeseries: {
    "1/22/20": { confirmed: 1, deaths: 0, recovered: 0 },
    "1/23/20": { confirmed: 1, deaths: 0, recovered: 0 },
    "1/24/20": { confirmed: 2, deaths: 0, recovered: 0 },
    "1/25/20": { confirmed: 2, deaths: 0, recovered: 0 },
    "1/26/20": { confirmed: 5, deaths: 0, recovered: 0 },
    "1/27/20": { confirmed: 5, deaths: 0, recovered: 0 },
    "1/28/20": { confirmed: 5, deaths: 0, recovered: 0 },
  },
};
let arr = [];
arr.push(a[`timeseries`]);
let dataFinal = [
  {
    name: "confirmed",
    series: [],
  },
  {
    name: "deaths",
    series: [],
  },
  {
    name: "recovered",
    series: [],
  },
];

let arrKeys = Object.keys(arr[0]);
for (let i = 0; i < arrKeys.length; i++) {
  let result = Object.keys(arr[0][arrKeys[i]]).map((key) => [
    String(key),
    arr[0][arrKeys[i]][key],
  ]);

  for (let j = 0; j < result.length; j++) {
    if (result[j][0] === "confirmed") {
      dataFinal[0].series.push({
        name: arrKeys[i],
        value: result[j][1],
      });
    }
    if (result[j][0] === "deaths") {
      dataFinal[1].series.push({
        name: arrKeys[i],
        value: result[j][1],
      });
    }
    if (result[j][0] === "recovered") {
      dataFinal[2].series.push({
        name: arrKeys[i],
        value: result[j][1],
      });
    }
  }
}
console.log(dataFinal);
// {
//     "name": "Iceland",
//     "series": [
//       {
//         "value": 3020,
//         "name": "2016-09-21T17:38:00.502Z"
//       },
//       {
//         "value": 4034,
//         "name": "2016-09-17T12:57:21.728Z"
//       },
//       {
//         "value": 3851,
//         "name": "2016-09-14T13:59:03.444Z"
//       },
//       {
//         "value": 6616,
//         "name": "2016-09-21T00:32:07.391Z"
//       },
//       {
//         "value": 3398,
//         "name": "2016-09-13T17:05:23.668Z"
//       }
//     ]
//   },
