package recursion;

public class factorial {
    public static int factorialNumber(int n) {
        if (n == 0) {
            return 1;
        }
        return n*(n-1);
    }
    public static void main(String args[]) {
        System.out.println(factorialNumber(3));
    }
}
