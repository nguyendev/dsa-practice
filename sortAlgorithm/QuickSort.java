package sortAlgorithm;

import java.util.Arrays;

public class QuickSort {
    public static void quickSort(int a[], int l, int r) {
        if (l >= r) {
            return;
        }
        //step 1 select key
        int key = a[(l+r)/2];
        //step 2 sort array follow key
        int k = partition(a, l, r, key);
        //Step 3 separate array
        quickSort(a, l, k-1);
        quickSort(a, k, r);
        
    }

    //return pivot value
    static int partition(int a[], int l, int r, int key) {
        int iL = l;
        int iR = r;
        while(iL <= iR) {
            //with iL will find  ele >= key to swap
            while(a[iL] < key) {
                iL++;
            }

            //with iR will fid ele <= key to swap 
            while (a[iR] > key) {
                iR--;
            }
            //swap two element is incorrect position
            if (iL <= iR) {
                int tmp = a[iL];
                a[iL] = a[iR];
                a[iR] = tmp;
                iL++; 
                iR--;
            }
        }
        return iL;
    }
    public static void main(String args[]) {
        int a[] = {6 ,7, 8, 5 ,4, 1, 2, 3};
        quickSort(a, 0, a.length - 1);
        System.out.println(Arrays.toString(a));
    } 
}
