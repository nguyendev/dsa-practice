package recursion;

public class FIbonanci {
    public static int fibonanciCal(int n) {
        if (n <= 2) {
            return 1;
        }
        System.out.println("can tinh: "+ n);
        return fibonanciCal(n-1)+fibonanciCal(n-2);
    }
    //recursive remember
    static int[] f = new int[1000];
    public static int fibonanciCal2(int n) {
        if (f[n] != 0) {
            return f[n];
        }
        System.out.println("can tinh: "+ n);
        if (n <= 2) {
            f[1] = 1;
            f[2] = 1;
            return 1;
        }
        f[n] = fibonanciCal2(n-1)+fibonanciCal2(n-2);
        return f[n];
    }
    public static void main(String args[]) {
        System.out.println(fibonanciCal(10));
    }
}
