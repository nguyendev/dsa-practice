public class Merge_Sorted_Array_88 {
    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        for (int num : nums2) {
            insetElementToArr(nums1, num, m);
            m++;
        }
    }

    public static void mergeTwoPoiter(int[] ai, int ni, int[] aj, int nj) {
        int i = ni - 1;
        int j = nj - 1;
        int k = ni + nj - 1;

        while (i >= 0 || j >= 0) {
            if (i >= 0 && j >= 0)// Ca i va j deu hop le
            {
                // Tim phan tu lon hon nhet vao mang ai
                if (ai[i] >= aj[j]) {
                    ai[k--] = ai[i--];
                } else {
                    ai[k--] = aj[j--];
                }
            } else if (i >= 0)// Chi co i hop le
            {
                ai[k--] = ai[i--];
            } else // Chi co j hop le
            {
                ai[k--] = aj[j--];
            }
        }

    }

    private static void insetElementToArr(int[] nums1, int num, int m) {
        boolean findK = false;
        for (int i = 0; i < m; i++) {
            if (nums1[i] > num) {
                findK = true;
                for (int j = m - 1; j >= i; j--) {
                    nums1[j + 1] = nums1[j];
                }
                nums1[i] = num;
                break;
            }
        }
        if (findK == false) {
            nums1[m] = num;
        }
    }

    public static void main(String[] args) {
        int[] nums1 = { 2, 0 };
        int[] nums2 = { 1 };
        mergeTwoPoiter(nums1, 1, nums2, 1);
        System.out.println("Done");
    }
}
