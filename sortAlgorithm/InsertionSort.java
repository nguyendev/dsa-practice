package sortAlgorithm;

import java.util.Arrays;

public class InsertionSort {
    public static void Sort(int[] a) {
        for (int i = 0; i < a.length; i++) {
            int j = i - 1;
            int ai = a[i];
            while (j >= 0 && a[j] > ai) {
                a[j+1] = a[j];
                j--;
            }
            a[j+1] = ai;
        }
        System.out.println(Arrays.toString(a));
    }
   public static void main(String args[]) {
       int[] arr = { 2, 1, 3, 7, 6 };
       Sort(arr); 
   } 
}
