public class HelloWorld {
    public static int calculateDigits(int num) {
        int amountDigit = 0;
        int rs = num;
        while (rs != 0) {
            rs = num / 10;
            num = rs;
            amountDigit++;
        }
        return amountDigit;
    }

    public static int FindEven(int[] nums) {
        int rs = 0;
        for (int i = 0; i < nums.length; i++) {
            int amountOfDigits = calculateDigits(nums[i]);
            if (amountOfDigits % 2 == 0) {
                rs++;
            }
        }
        return rs;
    }

    public static void main(String[] args) {
        int[] arr = { 12, 345, 2, 6, 7896 };
        System.out.println(FindEven(arr));
        System.out.println("Hello world");
    }
}

// solution 2
// class Solution {
//     public int findNumbers(int[] nums) {
//         int sum = 0;
//         for (int i = 0; i < nums.length; i++) {
//             String str = String.valueOf(nums[i]);
//             if (str.length() % 2 == 0)
//                 sum++;
//         }
//         return sum;
//     }
// }