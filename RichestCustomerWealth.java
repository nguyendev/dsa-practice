public class RichestCustomerWealth {
    public static int findSumWithoutUsingStream(int[] array) {
        int sum = 0;
        for (int value : array) {
            sum += value;
        }
        return sum;
    }
    public static int maximumWealth(int[][] accounts) {
        int rs = 0;
        for(int i = 0; i < accounts.length; i++) {
            int total = findSumWithoutUsingStream(accounts[i]);
            rs = Math.max(rs, total);
        }
        return rs;
    }
    public static void main(String[] args) {
        int[][] arr = {{1,2,3},{3,2,1}};
        System.out.println(maximumWealth(arr));
    }
}
