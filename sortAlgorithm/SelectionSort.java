package sortAlgorithm;

import java.util.Arrays;

public class SelectionSort {
    public static void Sort(int[] a) {
        int l = a.length;
        for (int i = 0; i < l; i++) {
            int minIdx = i;
            for (int j = i+1; j < l; j++) {
                if (a[minIdx] > a[j]) {
                    minIdx = j;
                }
            }
            if (minIdx != i) {
                int tmp = a[i];
                a[i] = a[minIdx];
                a[minIdx] = tmp;
            }
        }
        System.out.println(Arrays.toString(a));
    }
    public static void main(String args[]) {
        int[] arr = { 1, 2, 3, 7, 6 , 5};
        Sort(arr); 
    }
}
