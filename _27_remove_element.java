public class _27_remove_element {
    public static int removeElement(int[] nums, int val) {
        int n = nums.length;
        for (int i = 0; i < n;) {
            if (nums[i] == val) {
                for (int j = 0; j < n-1; j++) {
                    nums[j] = nums[j+1];
                }
                n--;
            } else {
                i++;
            }
        }
        return n;
    }
    public static void main(String[] args) {
        int[] nums = {1,2,6,3,4,5,6};
        System.out.println(removeElement(nums, 6));
    }
}
