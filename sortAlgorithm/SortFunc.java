package sortAlgorithm;

import java.util.Arrays;
import java.util.Comparator;

public class SortFunc {
    /**
     * InnerSortFunc
     */
    static class Student implements Comparable {
        String name;
        int age;
        public Student(String name, int age) {
            this.name = name;
            this.age = age;
        }
        @Override
        public String toString() {
            return "Student{" + "age=" + age + ", name='" + name + '\'' + '}';
        }

        @Override
        public int compareTo(Object other) {
            Student otherStudent = (Student)other;
            if (this.age == otherStudent.age) {
                return 0;
            } else if(this.age > otherStudent.age) {
                return 1;
            } else {
                return -1;
            }
        }
    }
    public static void main(String args[]) {
        // #1 sort an interger array
        int[] intArray = {3,2,1,0};
        Arrays.sort(intArray);
        System.out.println(Arrays.toString(intArray));

        // #2 sort a string array
        String[] stringArray = {"1","000","222"};
        Arrays.sort(stringArray);
        System.out.println(Arrays.toString(stringArray));

        // #3 sort an array with interface comparable
        Student[] students = {new Student("Eguyen", 20), new Student("asdasdasdaD", 10)};
        Arrays.sort(students);
        System.out.println(Arrays.toString(students));

        // #4 Sort an array  with comparator
        Arrays.sort(students, new Comparator<Student>(){
            @Override
            public int compare(Student before, Student after) {
                // 0 before == after
                // 1 before > after
                // -1 before < after

                if (before.name.length() == after.name.length()) {
                    return 0;
                } else if(before.name.length() > before.name.length()) {
                    return 1;
                } else {
                    return -1;
                }
                // return before.name.compareTo(after.name);

            }
        });
        System.out.println(Arrays.toString(students));
    }
}
