// var checkCommonLetter = function (str1, str2) {
//   let count = 0;
//   const obj = str2.split("");
//   if (str1.length > str2.length) {
//     let nA = str1.split("");
//     for (let index = 0; index < nA.length; index++) {
//       if (str2.includes(nA[index])) {
//         return false;
//       }
//     }
//   } else {
//     let nA = str2.split("");
//     for (let index = 0; index < nA.length; index++) {
//       if (str1.includes(nA[index])) {
//         return false;
//       }
//     }
//   }
//   return true;
// };
// var maxProduct = function (words) {
//   let selectIndex = 0;
//   let rs = 0;
//   let i = 0;
//   while (selectIndex < words.length) {
//     if (checkCommonLetter(words[selectIndex], words[i])) {
//       rs = Math.max(rs, words[selectIndex].length * words[i].length);
//     }
//     i++;
//     if (i === words.length) {
//       i = 0;
//       selectIndex++;
//     }
//   }
//   if (rs === 0) {
//     return 0;
//   }
//   return rs;
// };

var maxProduct = function (words) {
  let maxProd = 0;
  const codeA = "a".charCodeAt(0);

  function binCode(word) {
    return [...word].reduce((wordBin, letter) => {
      return wordBin | Math.pow(2, letter.charCodeAt(0) - codeA);
    }, 0);
  }

  const wordBins = words.reduce((bins, word) => {
    bins.set(word, binCode(word));
    return bins;
  }, new Map());

  const binWords = new Map();
  wordBins.forEach((binCode, word) => {
    if (!binWords.get(binCode) || word.length > binWords.get(binCode).length) {
      binWords.set(binCode, word);
    }
  });

  const list = [...wordBins.entries()];

  list.forEach(([longestWordA, binCodeA], idx) => {
    for (let i = idx + 1; i < list.length; i++) {
      const [longestWordB, binCodeB] = list[i];
      if ((binCodeA & binCodeB) === 0) {
        const prod = longestWordA.length * longestWordB.length;
        maxProd = Math.max(prod, maxProd);
      }
    }
  });

  return maxProd;
};
