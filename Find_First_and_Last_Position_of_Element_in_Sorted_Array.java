import java.util.Arrays;

public class Find_First_and_Last_Position_of_Element_in_Sorted_Array {
    public static int[] searchRange(int[] nums, int target) {
        int left = 0;
        int right = nums.length - 1;
        if (!(nums.length > 1)) {
          return new int[] {-1, -1};
        }
        if (Arrays.asList(nums).contains(target)) {
          return new int[] {-1, -1};
        }
        int firstPosition = 0;
        while (firstPosition != 0) {
          if (nums[left] == target) {
            firstPosition = left;
            break;
          }
          left++;
        }
        int lastPosition = 0;
        while (lastPosition != 0) {
          if (nums[right] == target) {
            lastPosition = right;
            break;
          }
          right--;
        }
        return new int[] {firstPosition, lastPosition};
    }
    public static void main(String[] args) {
        searchRange(new int[] {1,2,3,4,4,5}, 4);
    }
}
