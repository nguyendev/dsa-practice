package sortAlgorithm;

class BubleSort {
    public static void printArray(int n, int[] a) {
        System.out.printf("%d: ",n);
        for (int i = 0; i < a.length; i++) {
            System.out.printf("%d ",a[i]);
        }
        System.out.println();
    }
    public static void sort(int[] a) {
        int l = a.length;
        for (int i = 0; i < a.length; i++) {
            boolean isSorted = true;
            for (int j = 0; j < l - i - 1; j++) {
                if (a[j] > a[j+1]) {
                    int tmp = a[j];
                    a[j] = a[j+1];
                    a[j+1] = tmp;
                    isSorted = false;
                }
            }
            printArray(a[i], a);
            if (isSorted) {
                break;
            }
        }
    }

    public static void main(String args[]) {
        int[] arr = { 1, 2, 3, 7, 6 };
        sort(arr);
    }
}