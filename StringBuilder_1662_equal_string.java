public class StringBuilder_1662_equal_string {
    public static boolean arrayStringsAreEqual(String[] word1, String[] word2) {
        StringBuilder sb1 = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        for (String s : word1) {
            sb1.append(s);
        }
        for (String s : word2) {
            sb2.append(s);

        }
        return sb1.toString().equals(sb2.toString());
    }

    public static void main(String[] args) {
        String[] array1 = { "ab", "c" };
        String[] array2 = { "a", "bc" };
        System.out.println(arrayStringsAreEqual(array1, array2));
    }
}
