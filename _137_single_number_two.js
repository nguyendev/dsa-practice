var singleNumber = function(nums) {
    let myMap = new Map();
    for(let num of nums) {
        myMap.set(num, (myMap.get(num)|| 0) + 1)
    }
    for(let [key, val] of myMap)  {
        if(val === 1) return key;
    }
        
};
console.log(singleNumber([[2,2,3,2]]))