public class _Break_palindrome {
    public static String breakPalindrome(String palindrome) {
        int n = palindrome.length();
        if (n == 1) return "";
        System.out.println(n/2);
        char[] arr = palindrome.toCharArray();
        for (int i = 0; i < n/2; i++){
            if (palindrome.charAt(i) != 'a'){
                arr[i] = 'a';
                return String.valueOf(arr);
            }
        }
        for (int i = n - 1; i >= n/2; i--){
            if (palindrome.charAt(i) != 'b'){
                arr[i] = 'b';
                return String.valueOf(arr);
            }
        }
        return ""; 
    }
    public static void main(String[] args) {
        System.out.println(breakPalindrome("aba"));
    }
}