package recursion;

public class _36_50_Pow {
    public double myPow(double x, int n) {
        if (n == 0) {
            return 1;   
        }
        //10^-1 1/10
        int n2 = 0;
        if (n < 0) {
            n2 = n%2 == 0 ? -(n/2) : (-n-1)/2;
            x = 1/x;
        } else {
            n2 = n%2 == 0 ? n/2 : (n-1)/2;
        }
        double tmp = myPow(x, n2);
        return n%2 == 0 ? tmp * tmp : tmp * tmp * x;
    }
    public static void main(String args[]) {
        
    }
}
