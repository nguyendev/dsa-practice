package sortAlgorithm;

import java.util.Arrays;

public class ThirdMaximumNumber {
    public static void insert(long[] maxArray, int number) {
        int i = 0;
        while (i < maxArray.length) {
            if (maxArray[i] == number) {
                return;
            } else if (number > maxArray[i]) {
                break;
            } else {
                i++;
            }
        }
        if (i < maxArray.length) {
            for (int j = maxArray.length-2; j >= i; j--) {
                maxArray[j + 1] = maxArray[j];
            }
            maxArray[i] = number;
        }

    }

    public static int thirdMax(int[] nums) {
        long[] maxArray = { Long.MIN_VALUE, Long.MIN_VALUE, Long.MIN_VALUE };
        for (int i = 0; i < nums.length; i++) {
            insert(maxArray, nums[i]);
        }
        if (maxArray[2] == Long.MIN_VALUE) {
            return (int) maxArray[0];
        }
        System.out.println(Arrays.toString(maxArray));
        return (int) maxArray[2];
    }

    public static void main(String args[]) {
        int[] a = { 2, 2, 3, 1 };
        System.out.println(thirdMax(a));
    }
}
