package sortAlgorithm;

public class SortAnArray_leetCode {
    public static int[] mergeSort(int[] a, int l, int r) {
        if (l > r) {
            return new int[0];
        }
        if (l == r) {
            int[] singleEle = { a[l] };
            return singleEle;
        }
        int k = (l + r) / 2;
        // divide arr
        int[] arr1 = mergeSort(a, l, k);
        int[] arr2 = mergeSort(a, k + 1, r);

        // mix array a1 a2 is sorted arrays
        int n = arr1.length + arr2.length;

        int[] rs = new int[n];
        int i = 0;
        int i1 = 0;
        int i2 = 0;
        while (i < n) {
            if (i1 < arr1.length && i2 < arr2.length) { //a1 & a2 is not empty
                if (arr1[i1] <= arr2[i2]) { //arr2 greater
                    rs[i] = arr1[i1];
                    i++;
                    i1++;
                } else {
                    rs[i] = arr2[i2];
                    i++;
                    i2++;
                }
            } else { // arr1 or arr2 is empty
                if (i1 < arr1.length) {
                    rs[i] = arr1[i1];
                    i++;
                    i1++;
                } else {
                    rs[i] = arr2[i2];
                    i++;
                    i2++;
                }
            }
        }
        return rs;
    }

    public static int[] sortArray(int[] nums) {
        return mergeSort(nums, 0, nums.length - 1);
    }

    public static void main(String args[]) {
        int[] arr = { 1,5,7,4,8,3 };
        System.out.println(sortArray(arr));
    }
}
