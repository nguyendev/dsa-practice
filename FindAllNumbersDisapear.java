import java.util.ArrayList;
import java.util.List;

public class FindAllNumbersDisapear { 
    public List<Integer> findDisappearedNumbers(int[] nums) {
        List<Integer> rs = new ArrayList<Integer>();
        for (int i = 0; i < nums.length; i++) {
            int idx = Math.abs(nums[i])-1;
            nums[idx] = Math.abs(nums[idx]) * -1;
        }
        for (int j = 0; j < nums.length; j++) {
            if (nums[j] > 0) {
                rs.add(nums[j]);
            }
        }
        return rs;
    }
    public static void main(String args[]) {
        
    }
}
