public class SampleString {
    // String is a a collection of char
    // toCharArray to convert string to array
    public static void main(String[] args) {
        String s1 = "Hello world!!! 1,2,2,3";
        String s2 = new String("Hello world");
        char myChar = 'B';
 
        for (int i = 0; i < s1.length(); i++) {
            char c = s1.charAt(i);
            if (Character.isDigit(c)) {
                System.out.println(c);
            }      
            
            if (c >= 48 && c <= 59) {
                System.out.println(c);
            }
        }
    }
}
