function longestPalindrome(s) {
    if(s.length < 2) return s
    let currentLongestSubstring = s[0]
    for(let i = 0; i< s.length; i++){
        for(let j = s.length; j>i; j--){
            const curr = s.slice(i, j)
            if(curr.length > currentLongestSubstring.length && isPalindrome(curr)){
                currentLongestSubstring = curr
            }
                
        }
    }
    return currentLongestSubstring
};

const isPalindrome = (subSt) => {
    let start = 0
    let end = subSt.length - 1
    while(start<=end){
        if(subSt[start] === subSt[end]){
            start++
            end--
        }
        else{
            return false
        }
    }
    return true
}