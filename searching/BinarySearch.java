package searching;

public class BinarySearch {
    public static int binarySearch(int[] a, int n) {
        int left = 0;
        int right = a.length - 1;
        while (left < right) {
            int middle = (left + right) / 2;
            if (n == a[middle]) {
                return middle;
            } else if (n > a[middle]) {
                left = middle + 1;
            } else {
                right = middle - 1;
            }
        }
        return -1;
    }

    public static int bSearchRecursive(int[] a, int n, int left, int right) {

        int middle = (left + right) / 2;
        if (left > right) {
            return -1;
        }
        if (n == a[middle]) {
            return middle;
        } else if (n > a[middle]) {
           return bSearchRecursive(a, n, middle+1, right);
        } else {
            return bSearchRecursive(a, n, left, middle-1);
        }
 
       
    }

    public static int searchRecursive(int[] a, int target) {
        int len = a.length;
        return bSearchRecursive(a, target, 0, len-1);
    }

    public static void main(String args[]) {
        int[] arr = { -1, 0, 3, 5, 9, 12 };
        System.out.println(searchRecursive(arr, 9));
    }
}
